<%@taglib prefix="body" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<body:wrapper>

    <p class="lead text-right">
        Шесть десятилетий назад Москва неожиданно оказалась на пороге эпидемического кризиса: дал знать о себе позабытый убийца человечества — черная оспа. Чтобы спасти миллионы людей, в СССР прибегли к беспрецедентным мерам. Невероятная по своим масштабам операция позволила всего за несколько дней привить 10 млн человек и остановить потенциального убийцу менее чем за три недели. Вот как это было.
    </p>

    <h3>Черная оспа — убийца миллионов. Как столицу СССР за несколько дней спасли от вымирания</h3>
    <h5> Портрет убийцы </h5>
    Не только чума считалась напастью и наказанием человечества. Черная оспа — эта зараза хитрее своей смертоносной сестрицы и куда опаснее.

    Считается, что черная оспа преследует человечество на протяжении всей его истории. Вероятно, косвенные намеки на нее встречаются в Библии. Однако первое документально засвидетельствованное опустошительное влияние инфекции было зафиксировано в IV веке. Болезнь распространилась по всей Азии и убила треть японцев, особенно тщательно выкашивая города, в которых погибало до 70% населения.

    <div class="card mb-3">
        <img class="card-img-top" src="/lesson10/images/ab1a632ae8a5c3c6ec39472e062cf5e7.jpeg" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title"></h5>
            <p class="card-text">Японский манускрипт с описанием оспы</p>
            <p class="card-text"><small class="text-muted"></small></p>
        </div>
    </div>

    Не прошло и века, как оспа добралась до Европы. Смертность была ниже, чем от чумы, «всего» 40 процентов вместо 97. Однако черная оспа, в отличие от масштабных вспышек бубонной чумы, никогда не оставляла людей. Постоянно появлялись локальные очаги заражения оспой, при этом она никогда не исчезала, а лишь слегка затухала.

    К периоду позднего Средневековья люди настолько свыклись с постоянным присутствием в своей жизни этой инфекции, что перестали обращать на нее особое внимание. К оспе стали относиться как к простуде, но от которой ты можешь умереть или остаться калекой. Считается, что к XV веку оспой переболел чуть ли не каждый житель Западной Европы.

    <div class="card mb-3">
        <img class="card-img-top" src="/lesson10/images/b6378f1d1460dc562a69279cce4f7c45.jpeg" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title"></h5>
            <p class="card-text">Ацтекский рисунок XVI века</p>
            <p class="card-text"><small class="text-muted"></small></p>
        </div>
    </div>



    Чума опустошала мир во времена своих относительно редких визитов, оспа же была рядом с людьми всегда. Дамоклов меч, который карает всякого и от которого не сбежать. Фатум, ставший спутником королей и чистильщиков выгребных ям, вельмож и крестьян, азиатов и европейцев. А в XVI веке оспа благодаря европейским завоеваниям прочно обосновалась в Америке, где выкосила миллионы местных жителей.

    Средневековые врачи в какой-то момент даже перестали описывать симптомы черной оспы в своих трудах. Болезнь была настолько знакома всем, что лишний раз фиксировать очевидное ее проявление было бо́льшим моветоном, чем сегодня от души чихнуть, не прикрыв рот. Среди криминалистов в качестве особой приметы широко использовалось указание: отсутствие признаков оспы. В свою очередь, благодаря этому широкое распространение получила косметика, которой можно было замаскировать оспенные шрамы.

    Жениться предпочитали на девушках с оспинами на лице. Это была гарантия того, что больше она уже никого не заразит и не умрет от оспы.
    <div class="card mb-3">
        <img class="card-img-top" src="/lesson10/images/ea948c27088be95ad9f08d50e873f6c1.jpeg" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title"></h5>
            <p class="card-text">Противооспенная вакцинация</p>
            <p class="card-text"><small class="text-muted"></small></p>
        </div>
    </div>


    Сегодня известно, что возбудителем черной оспы является вирус Variola семейства Poxviridae. Распространяется он воздушно-капельным путем, а также при прикосновении к больному или вещам, с которыми тот контактировал. Характеризуется высокой заразностью и устойчивостью к воздействию окружающей среды. Вирус любит сухую и холодную погоду, может сохранять жизнеспособность месяцами и даже годами.

    Инкубационный период длится от одной до двух недель. Инфекция дает о себе знать повышением температуры тела, сильными ломящими болями в конечностях и ознобом. Через пару дней появляется сыпь на коже по всему телу, даже на слизистых оболочках. Болезнь сопровождается нарушением сознания, судорогами, могут наблюдаться многочисленные кровоизлияния в кожу на месте сыпи. Если повезет и человек выживет, у него на коже на месте нагноившихся пузырьков остаются многочисленные рубцы. В качестве осложнений часто встречались слепота и воспаление мозга.


    Разумеется, никаких лекарств от оспы не существовало. Правда, люди давно додумались вводить небольшое количество оспенного гноя из сыпи в качестве прививки. Недостаток такого метода вакцинации — довольно высокая смертность и стимуляция появления новых очагов эпидемии.

    Только в первой половине XX века черная оспа убила около полумиллиарда человек. И все же к середине прошлого столетия опаснейшую заразу удалось остановить благодаря усовершенствованным методам массовой вакцинации.

    <div class="card mb-3">
        <img class="card-img-top" src="/lesson10/images/ff8ddd622066eb226a5e10bea7b8d139.jpeg" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title"></h5>
            <p class="card-text">1901 год. Мальчик справа привит, а слева — нет</p>
            <p class="card-text"><small class="text-muted"></small></p>
        </div>
    </div>



    <h5>«Подарок» советским гражданам из Индии</h5>
    Считается, что окончательно победить оспу удалось в 1980 году. Сегодня штаммы вируса хранятся в парочке лабораторий. Стоит отметить, что в большинстве стран с оспой справились гораздо раньше. Инфекцию в СССР, например, «репрессировали» в 1936-м. Разумеется, спустя 20 лет пережившие Вторую мировую люди о ней совсем позабыли. Оспа оставалась страшной сказкой из экзотических стран, которой разве что пугали детей. Вряд ли кто-то мог предположить, что во времена запуска спутников и подготовки полета в космос человека о себе может дать знать старинная зараза.

    Но случилось то, к чему было сложно подготовиться заранее. Причиной угрозы заражения миллионов людей стал Алексей Кокорекин — надежный товарищ советской системы. В марте 1959 года ему, известному мастеру агитплакатов («Смерть фашистской гадине!», «За Родину!» — это все его работы), лауреату двух сталинских премий и заслуженному деятелю искусств РСФСР, исполнилось 53 года. До очередного дня рождения художнику дожить было не суждено, но пока он об этом не знал.


    Кокорекин готовился к большому событию: родная партия пообещала командировку в экзотическую Индию. В составе делегации писателей и художников из холодной декабрьской Москвы он отправился в незабываемое путешествие. Две недели пролетели как одно яркое мгновение.


    За время командировки Алексею больше всего запомнилась масштабная траурная церемония в городе Варанаси. Там среди прочих хоронили местного брамина. Говорят, Кокорекин даже купил что-то из вещей жреца в качестве сувенира. Событие очень важное, так как, скорее всего, именно в этот момент советский художник заразился.

    Необходимо отметить, что в то время все граждане проходили противооспенную вакцинацию. Кокорекин тоже прошел ее за год до командировки. Перед отбытием в Индию он должен был повторно привиться. Мнения расходятся, но большинство медиков считают, что Алексею каким-то образом удалось избежать процедуры, при этом заполучив необходимое разрешение врачей.

    <div class="card mb-3">
        <img class="card-img-top" src="/lesson10/images/37834571a210dc440cc011489cec3cd4.jpeg" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title"></h5>
            <p class="card-text">Шитала Мара — индуистская богиня оспы. Она не только насылала болезнь, но и лечила ее</p>
            <p class="card-text"><small class="text-muted"></small></p>
        </div>
    </div>



    23 декабря 1959 года отдохнувший и полный впечатлений дважды лауреат сталинских премий вернулся в Москву. В аэропорту Внуково его встречали вторая жена, дочь от первого брака и несколько близких друзей. Кокорекин выглядел неважно. Впрочем, пока недомогание списывали на долгий перелет и акклиматизацию.

    Но к вечеру того же дня состояние заслуженного деятеля резко ухудшилось: поднялась температура, появился тяжелый надрывный кашель, очень сильно болела поясница. В поликлинике Кокорекину моментально поставили диагноз «грипп». Все же понятно: человек прилетел из теплой страны, а тут вам сразу холод, эпидемия гриппа.

    <div class="card mb-3">
        <img class="card-img-top" src="/lesson10/images/9b76e151c90328094d567ccb7cbd4839.jpeg" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title"></h5>
            <p class="card-text">Вирус оспы</p>
            <p class="card-text"><small class="text-muted"></small></p>
        </div>
    </div>


    Однако спустя день состояние еще ухудшилось. Антибиотики не помогали, жаропонижающие «не работали». 27 декабря Алексея Кокорекина отвезли в московскую Боткинскую больницу. К тому времени на теле художника появилась характерная сыпь. По словам очевидцев, во время осмотра молодая врач несмело предположила оспу. Однако маститый профессор высмеял юную коллегу и отправил Кокорекина в отделение, где лежали больные гриппом. Сыпь же списали на аллергическую реакцию на антибиотики.

    Спустя два дня всем стало ясно, что Кокорекин умрет. Никто не знал, что делать. Вроде бы грипп, но лечению совершенно не поддается. Мужчина стал задыхаться, у него зафиксировали отек легких. 29 декабря Алексей умер в окружении консилиума врачей, которые лишь беспомощно разводили руками.


    Наверное, если бы умер не известный художник, приближенный к Кремлю, а обычный москвич, его бы просто похоронили, а в диагнозе написали бы что-нибудь вроде «осложнения ОРВИ». Что бы тогда случилось, вообразить страшно. Скорее всего, СССР ожидала бы невиданная вспышка смертельно опасной эпидемии.

    <h5>Столица на карантине</h5>
    Это был тот случай, когда приближенность Кокорекина к власти сыграла на руку не только его родственникам, но и всей стране. Умер видный человек, поэтому хочешь не хочешь, а докапываться до истинной причины смерти было нужно.

    В ходе вскрытия стало ясно: что-то не так. Под вопросом даже поставили чуму. На всякий случай Кокорекина решили кремировать. В крематорий его привезли в закрытом гробу, в автобус, который перевозил тело, никого, кроме водителя, не пустили.


    На всякий случай в НИИ, занимающемся исследованием вирусов, отправили образцы тканей художника. Одновременно в Боткинской больнице, где лежал Алексей, у пациентов стали появляться те же признаки болезни, что были у него. Впрочем, пока все предпочитали списывать их на сыпь от аллергии.

    Однако уже 15 января неожиданная весть накрыла медиков. Работавший в НИИ академик Морозов только бросил взгляд в микроскоп, как моментально вынес вердикт: натуральная оспа.

    Переполох был страшный. В столице СССР объявился давно, как казалось, уничтоженный враг — смертоносная и живучая инфекция. Лекарства от нее не существует, человек либо погибает, либо выздоравливает. Причем шансов выбраться из этой переделки невредимым практически нет. Усложнял положение тот факт, что с момента завоза болезни прошло три недели!


    И тогда заработала обычно неповоротливая, но порой выходящая из анабиоза советская государственная машина. В первую очередь остановили авиационное, автомобильное и железнодорожное сообщение с Москвой, город буквально отрезали от страны. Ввели казарменное положение в Боткинской больнице — никто не мог войти и выйти. Запертыми оказались около 5 тыс. пациентов, врачей и обслуживающего персонала.

    В дело вступили армия, КГБ и милиция. За несколько дней предстояло вычислить все контакты Кокорекина с момента его посадки на рейс из Дели в Москву. На минуточку, в то время не просто интернета не было — не существовало разветвленной сети камер наблюдения, а телефонная связь почиталась за высшее достижение человечества.

    Москву наводнили сотни автомобилей скорой помощи с докторами в противочумных комбинезонах. Они ехали по адресам контактов, которые удавалось установить милиции. А количество таких контактов росло как на дрожжах. Нашли всех пассажиров самолета, с которыми летел Кокорекин, в карантин забрали таможенников и семью таксиста, стюардесс и пилотов.


    Кого-то снимали с поезда. Один из знакомых художника отправился в Париж — самолет развернули в воздухе, всех пассажиров доставили в инфекционную больницу. Туда же попала преподавательница, которая принимала зачет у дочери Кокорекина. Заодно «приняли» еще 120 студентов из той же и параллельных групп. Врач, который принимал художника в поликлинике, с тех пор успел осмотреть более 100 пациентов — всех их вместе с семьями тоже доставили в больницы.

    Выяснилось, что жена Алексея успела отнести в комиссионку подарки и сувениры, которые он привез из Индии. Пришлось искать покупателей. Оказалось, что заразиться успели многие друзья Кокорекина и члены их семей. Оспу подхватил страховой агент, заглянувший в квартиру художника после его смерти. В больнице на этаж выше над палатой Алексея заболел мальчик: инфекция пробралась через вентиляционный канал. Еще один пациент вообще находился в другом крыле больницы, но заразился, коснувшись халата врача. По коридору мимо умирающего Кокорекина прошел истопник — в итоге он заболел и умер. От телефонной трубки, которую трогал один из контактировавших с художником медик, вирус подхватила сотрудница регистрации.

    Казалось, этому конвейеру контактировавших и зараженных не будет конца. Вот-вот — и ситуация выйдет из-под контроля. Сотрудники скорых работали без сна и отдыха, бесконечно допрашивались сотни людей, приходилось рассказывать о любовницах и любовниках, рушились целые семьи.

    <div class="card mb-3">
        <img class="card-img-top" src="/lesson10/images/20f2f7ba2edc4284ac21289fff46dedb.jpeg" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title"></h5>
            <p class="card-text"></p>
            <p class="card-text"><small class="text-muted"></small></p>
        </div>
    </div>


    Разумеется, все старались держать в тайне. Оккупировавшим предместья Боткинской больницы родственникам медперсонала и больных ничего не говорили. А когда что-то происходит, но никто не говорит, что именно, обязательно появляются слухи. По Москве из уст в уста стали передаваться рассказы «очевидцев» о тысячах цинковых гробов, массовых захоронениях и таинственной болезни.

    Тем временем в карантин поместили около 10 тыс. человек. В больницах мест для всех не хватило, поэтому некоторые оставались дома под строгим запретом не выходить даже на лестничную площадку.

    А потом началась грандиозная противооспенная операция. До конца января надо было вакцинировать почти 10 млн жителей Москвы и Подмосковья. Невероятная по своей масштабности и сложности задача было выполнена менее чем за неделю.

    <div class="card mb-3">
        <img class="card-img-top" src="/lesson10/images/7580c178776c9a9b8f71256840a580c4.jpeg" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title"></h5>
            <p class="card-text"></p>
            <p class="card-text"><small class="text-muted"></small></p>
        </div>
    </div>


    Самолетами в Москву доставили 10 млн доз вакцины. Было организовано 10 тыс. прививочных бригад из 30 тыс. медиков. На помощь врачам позвали всех, кто имел хоть какое-то отношение к медицине, — начиная от фельдшеров и заканчивая первокурсниками медвузов. Прививали всех подряд: младенцев и завсегдатаев ЛТП, умирающих и приезжих. От медиков не скрылся никто.

    Ежедневно в Москве и пригороде вакцинировали по 1,5 млн человек! В течение шести дней бешеной гонки со смертью все было кончено. Врачи одержали победу — к середине февраля вспышка черной оспы в столице СССР была ликвидирована. На все про все ушло меньше месяца, а вакцинацию миллионов человек и вовсе провели за несколько дней. Официально выявили 45 зараженных, из которых, включая Кокорекина, умерли четверо.

    Что ж, наверное, для человека и правда нет ничего невозможного. Потенциально 60 лет назад в Москве могла разразиться масштабнейшая эпидемия древней болезни, способная опустошить мегаполис с миллионами жителей. Стечение обстоятельств вкупе с чрезвычайной мобилизацией всех возможных сил помогли избежать катастрофы. А спустя еще 20 лет противооспенную вакцинацию в СССР и в остальном мире отменили. Главную напасть человечество одолело спустя десятки тысяч лет сосуществования.

</body:wrapper>
