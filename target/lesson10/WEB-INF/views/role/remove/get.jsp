<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="body" tagdir="/WEB-INF/tags" %>

<body:wrapper>

    <h2 class="text-muted">Удаление роли</h2>
    <h5>
        Вы действительно хотите удалить роль?
    </h5>
    <form action="../remove/" method="post">


        <div class="form-group">
            <label for="exampleInputRole">Наименование роли</label>
            <input type="text" name="name" value="${role.name}" readonly class="form-control" id="exampleInputRole"
                   aria-describedby="roleHelp">
            <small id="roleHelp" class="form-text text-muted"></small>
        </div>
        <input type="hidden" name="id" value="${role.id}">

        <button type="submit" class="btn btn-primary btn-block" value="submit">Да</button>


    </form>

</body:wrapper>