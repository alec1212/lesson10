<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="body" tagdir="/WEB-INF/tags" %>

<body:wrapper>

    <h2 class="text-muted">Редктирование пользователя</h2>
    <h5>
        Измените данные в полях ввода
    </h5>
    <form action="../edit/" method="post">

        <div class="form-group">
            <label for="exampleInputLogin">Логин</label>
            <input type="login" name="login" value="${user.login}" class="form-control" id="exampleInputLogin"
                   aria-describedby="loginHelp" required>
            <small id="loginHelp" class="form-text text-muted"></small>
        </div>

        <div class="form-group">
            <label for="exampleInputPassword">Пароль</label>
            <input type="password" name="pass" class="form-control" id="exampleInputPassword"
                   aria-describedby="passwordHelp" required>
            <small id="passwordHelp" class="form-text text-muted">Сохраняйте пароли втайне</small>
        </div>

        <div class="form-group">
            <label for="exampleInputRePassword">Повтор ввода пароля</label>
            <input type="password" name="repPass" class="form-control" id="exampleInputRePassword"
                   aria-describedby="rePasswordHelp" required>
            <small id="rePasswordHelp" class="form-text text-muted">Необходимо для подтверждения правильности
                ввода</small>
        </div>

        <input type="hidden" name="id" value="${user.id}">


        <h3 class="text-muted">Список ролей</h3>
        <table class="table">
            <thead>

            <tr>
                <th scope="col"></th>
                <th scope="col">id</th>
                <th scope="col">Наименование</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach items="${roles}" var="role">
                <tr>
                    <td>
                        <input type="checkbox" value=${role.id} ${role.isChecked?"checked":""}  name="isChecked"/>
                        <input type="hidden" value=${role.id}                              name="isChecked"/>
                    </td>
                    <th scope="row">${role.id}</th>
                    <td>${role.name}</td>
                </tr>
            </c:forEach>
            </tbody>

        </table>

        <button type="submit" class="btn btn-primary btn-block" value="submit">Да</button>


    </form>


</body:wrapper>