<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="body" tagdir="/WEB-INF/tags" %>

<body:wrapper>
    <h2 class="text-muted">Удаление пользователя</h2>
    <h5>
        Вы действительно хотите удалить пользователя?
    </h5>
    <form action="../remove/" method="post">
        login: <input type="login" name="login" readonly value="${user.login}">

        <div class="form-group">
            <label for="exampleInputLogin">Логин</label>
            <input type="login" name="login" value="${user.login}" class="form-control" id="exampleInputLogin"
                   aria-describedby="loginHelp">
            <small id="loginHelp" class="form-text text-muted"></small>
        </div>

        <input type="hidden" name="id" value="${user.id}">

        <button type="submit" class="btn btn-primary btn-block" value="submit">Да</button>


    </form>


</body:wrapper>