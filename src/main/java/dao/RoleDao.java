package dao;

import entities.Role;
import entities.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class RoleDao {

    @PersistenceContext
    private EntityManager em;

    public Boolean isExist(final String name)
    {
        if (name == null || name.isEmpty()) return false;
        TypedQuery<Role> existLoginQuery  = em.createQuery("FROM Role WHERE name = :name", Role.class);
        existLoginQuery.setParameter("name", name);
        List<Role> existRoles = existLoginQuery.getResultList();
        if (existRoles.size() > 0)
            return true;
        else
            return false;
    }

    public Boolean isExist(final Role role)
    {
        Role dbRole =  em.find(Role.class, role.getId());
        if(dbRole != null)
            return true;
        else
            return false;
    }

    public Role createRole (final String name)  {
        if (name == null || name.isEmpty()) return null;
        if (isExist(name)) return null;

        Role role = new Role();
        role.setName(name);
        em.persist(role);
        return role;
    }

    public Role mergeRole (final Role role ) {
        if(!isExist(role)) return null;
        em.merge(role);
        return role;
    }

    public Role getRole (String name)
    {
        System.out.println("em = '" + em + "'");

        if (name == null || name.isEmpty()) return null;
        TypedQuery<Role> existRoleQuery  = em.createQuery("FROM Role WHERE name = :name", Role.class);
        existRoleQuery.setParameter("name", name);
        Role existRole = existRoleQuery.getSingleResult();
        return existRole;
    }

    public Role removeRole (final Role role ) {
        if(!isExist(role)) return null;
        em.remove(role);
        return role;
    }


    public Role getRoleById (String id)
    {
        if (id == null || id.isEmpty()) return null;
        TypedQuery<Role> existLoginQuery  = em.createQuery("FROM Role WHERE id = :id", Role.class);
        existLoginQuery.setParameter("id", id);
        Role existRole = existLoginQuery.getSingleResult();
        return existRole;
    }

    public List<Role> findAll()
    {
        return em.createQuery("FROM Role",Role.class).getResultList();
    }

}
