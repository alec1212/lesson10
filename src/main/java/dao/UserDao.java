package dao;

import entities.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Stateless
public class UserDao {

    @PersistenceContext
    private EntityManager em;

    public Boolean isExist(final String login)
    {
        if (login == null || login.isEmpty()) return false;
        TypedQuery<User> existLoginQuery  = em.createQuery("FROM User WHERE login = :login", User.class);
        existLoginQuery.setParameter("login", login);
        List<User> existUsers = existLoginQuery.getResultList();
        if (existUsers.size() > 0)
            return true;
        else
            return false;
    }

    public Boolean isExist(final User user)
    {
        User dbUser =  em.find(User.class, user.getId());
        if(dbUser != null)
            return true;
        else
            return false;
    }

    public User createUser (final String login, final String password) throws NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (isExist(login)) return null;

        User user = new User();
        user.setLogin(login);
        user.setHashPassword(password);
        em.persist(user);
        return user;
    }

    public User mergeUser (final User user ) {
        if(!isExist(user)) return null;
        em.merge(user);
        return user;
    }

    public User removeUser (final User user ) {
        if(!isExist(user)) return null;
        em.remove(user);
        return user;
    }


    public User getUserByLogin(String login)
    {
        if (login == null || login.isEmpty()) return null;
        TypedQuery<User> existLoginQuery  = em.createQuery("FROM User WHERE login = :login", User.class);
        existLoginQuery.setParameter("login", login);
        User existUser = existLoginQuery.getResultStream().findFirst().orElse(null);
        return existUser;
    }

    public User getUserById (String id)
    {
        if (id == null || id.isEmpty()) return null;
        TypedQuery<User> existLoginQuery  = em.createQuery("FROM User WHERE id = :id", User.class);
        existLoginQuery.setParameter("id", id);
        User existUser = existLoginQuery.getSingleResult();
        return existUser;
    }

    public List<User> findAll()
    {
        return em.createQuery("FROM User").getResultList();
    }

}
