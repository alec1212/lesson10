package servlets.role;

import entities.Role;
import entities.User;
import services.RoleService;
import services.UserService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet({"role/remove/*"})
public class RoleRemove extends HttpServlet {

    @Inject
    private services.RoleService roleService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");
        if(id==null) return;
        Role role = roleService.getRoleById(id);
        if(role==null) return;
        req.setAttribute("role", role);
        req.getRequestDispatcher("/WEB-INF/views/role/remove/get.jsp").forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");
        if(id==null) return;
        roleService.removeRole(id);
        resp.sendRedirect("../list");

    }
}
