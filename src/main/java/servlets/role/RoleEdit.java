package servlets.role;

import dto.RoleDto;
import entities.Role;
import entities.User;
import lombok.SneakyThrows;
import services.RoleService;
import services.UserService;
import utils.HtmlUtil;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet({"role/edit/*"})
@Transactional
public class RoleEdit extends HttpServlet {

    @Inject
    private RoleService roleService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");
        if (id == null) return;
        Role role = roleService.getRoleById(id);
        req.setAttribute("role", role);

        req.getRequestDispatcher("/WEB-INF/views/role/edit/get.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String name = req.getParameter("name");

        if ((name == null || name.isEmpty())) {
            resp.sendRedirect("../role/edit/?id=" + id);
            return;
        }

        roleService.editRole (id, name);

        resp.sendRedirect("../list");

    }
}
