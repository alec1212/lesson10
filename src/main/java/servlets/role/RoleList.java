package servlets.role;

import entities.Role;
import entities.User;
import lombok.SneakyThrows;
import services.RoleService;
import services.UserService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet({"role/list"})
public class RoleList extends HttpServlet {

    @Inject
    private RoleService roleService;

    @Override
    @SneakyThrows
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        List<Role> roles = roleService.getRoleList();
        req.setAttribute("roles",roles);
        System.out.println(String.format("Users: %s", roles));
        req.getRequestDispatcher("/WEB-INF/views/role/list/get.jsp").forward(req,resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)  {

       doGet(req,resp);
    }
}
