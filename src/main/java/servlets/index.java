package servlets;

import org.apache.commons.io.FilenameUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//@WebServlet({"/"})
//@WebServlet(urlPatterns = {"/"})
public class index extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String extension = FilenameUtils.getExtension(req.getRequestURI());
        System.out.println("request = "+ req.getRequestURI());
        req.getRequestDispatcher("/WEB-INF/views/index/index.jsp").forward(req,resp);
    }
}
