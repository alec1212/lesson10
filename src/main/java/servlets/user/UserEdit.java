package servlets.user;

import dto.RoleDto;
import entities.Role;
import entities.User;
import lombok.SneakyThrows;
import services.RoleService;
import services.UserService;
import utils.HtmlUtil;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet({"user/edit/*"})
@Transactional
public class UserEdit extends HttpServlet {

    @Inject
    private UserService userService;

    @Inject
    private RoleService roleService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");
        if (id == null) return;
        User user = userService.getUserById(id);
        req.setAttribute("user", user);
        List<Role> userRoles = user.getRoles();
        List<Role> allRoles = roleService.getRoleList();
        List<RoleDto> allRolesDto = allRoles.stream()
                .map(RoleDto::new)
                .collect(Collectors.toList());

        List<String> userRolesId = userRoles.stream()
                .map(ur -> ur.getId())
                .collect(Collectors.toList());

        allRolesDto.forEach(role -> {
            if (userRolesId.contains(role.getId()))
                role.setIsChecked(true);
        });

        req.setAttribute("roles", allRolesDto);
        req.getRequestDispatcher("/WEB-INF/views/user/edit/get.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String login = req.getParameter("login");
        String pass = req.getParameter("pass");
        String repPass = req.getParameter("repPass");
        Map<String, Boolean> checkDataOfRoles = HtmlUtil.GetCheckBoxMap(Arrays.asList(req.getParameterValues("isChecked")));


        if ((login == null || login.isEmpty())
                || (!repPass.equals(pass))) {
            resp.sendRedirect("../edit/?id=" + id);
            return;
        }

        userService.editUser(id, login, pass);
        userService.setRoles(id, checkDataOfRoles.entrySet()
                .stream()
                .filter(role -> role.getValue() == true)
                .map(role -> role.getKey())
                .collect(Collectors.toList()));
        resp.sendRedirect("../list");

    }
}
