package servlets.user;

import dao.UserDao;
import entities.User;
import services.UserService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet({"user/remove/*"})
public class UserRemove extends HttpServlet {

    @Inject
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");
        if(id==null) return;
        User user = userService.getUserById(id);
        if(user==null) return;
        req.setAttribute("user", user);
        req.getRequestDispatcher("/WEB-INF/views/user/remove/get.jsp").forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");
        if(id==null) return;
        userService.removeUser(id);
        resp.sendRedirect("../list");

    }
}
