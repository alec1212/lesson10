package servlets.user;

import lombok.SneakyThrows;
import services.UserService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet({"user/create"})
public class UserCreate extends HttpServlet {

    @Inject
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher("/WEB-INF/views/user/create/get.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String pass = req.getParameter("pass");
        String repPass = req.getParameter("repPass");

        Map<String,String> errors = new HashMap<>();

        if (login == null || login.isEmpty()) errors.put("login", "Логин не должен быть пустым");
        if(userService.isExistUserByLogin(login)) errors.put("login", errors.getOrDefault("login", "") + "; Пользователь с таким логином существует в базе");
        if (pass == null || pass.isEmpty()) errors.put("pass", "Пароль не должен быть пустым");
        if (!repPass.equals(pass)) errors.put("repPass", "Павторный ввод пароля не идентичен первоначальному");

        if (!errors.isEmpty()){
            req.setAttribute("login", login);
            req.setAttribute("errors", errors);
            req.getRequestDispatcher("/WEB-INF/views/user/create/get.jsp").forward(req, resp);
        }
        else {
            userService.createUser(login, pass);
            resp.sendRedirect("/lesson10/user/list");
        }


    }
}
