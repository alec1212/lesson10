package servlets;


import exceptions.UserNotFoundException;
import lombok.SneakyThrows;
import services.UserService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("auth/*")

public class auth extends HttpServlet {

    @Inject
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Object authStringObject = session.getAttribute("authString");
        final String pageFrom = req.getParameter("pageFrom");
        if (authStringObject != null) {
            String authString = authStringObject.toString();
            if (authString == "123") {
                resp.sendRedirect(pageFrom);
            }
        }
        Map<String,String> errors = new HashMap<String, String>();

        req.setAttribute("pageFrom", pageFrom);
        req.setAttribute("errors", errors);

        req.getRequestDispatcher("/WEB-INF/views/auth/GetAuth.jsp").forward(req, resp);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {

        final String login = req.getParameter("login");
        final String pass = req.getParameter("pass");

        Map<String, String> errors = new HashMap<String, String>();

        Boolean isExistUser = userService.isExistUserByLogin(login);
        Boolean isTruePassword = null;
        try {
            isTruePassword = userService.isTruePassword(login, pass);
            if (!isTruePassword) errors.put("pass", "Не верный пароль");

        } catch (UserNotFoundException e) {
            errors.put("pass", e.getMessage());
        }
        if (!isExistUser) errors.put("login", "Пользователь не существует");

        final String pageFrom = req.getParameter("pageFrom");
        if (errors.isEmpty()) {

            HttpSession session = req.getSession();
            session.setAttribute("authString", "123");
            String redirectUrl = "/lesson10";
            if (pageFrom != null) redirectUrl = pageFrom;
            resp.sendRedirect(redirectUrl);

        } else {

            req.setAttribute("pageFrom", pageFrom);
            req.setAttribute("login", login);
            req.setAttribute("errors", errors);
            req.getRequestDispatcher("/WEB-INF/views/auth/GetAuth.jsp").forward(req, resp);
        }

    }
}