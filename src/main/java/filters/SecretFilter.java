package filters;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter({"/secret/*"})
public class SecretFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        HttpSession httpSession = httpServletRequest.getSession();
        Object authStringObject = httpSession.getAttribute("authString");
        if (authStringObject != null) {
            String authString = authStringObject.toString();
            if (authString == "123") {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        } else {
            String pageFrom = httpServletRequest.getRequestURI();
            httpServletResponse.sendRedirect(String.format("/lesson10/auth/?pageFrom=%s", pageFrom));
        }


    }
}
