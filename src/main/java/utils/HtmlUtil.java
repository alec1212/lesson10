package utils;

import lombok.var;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class HtmlUtil {

    public static Map<String, Boolean> GetCheckBoxMap(List<String> checkboxParamList){

        Map<String, Long> result0 =
                checkboxParamList.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting() ));
        Map<String, Boolean> result = new HashMap<String, Boolean>();

        for (var i : result0.keySet())
        {
            if(result0.get(i) > 1)
            {
                result.put(i, true);
            }
            else
            {
                result.put(i, false);

            }
        }
        return result;
    }

}
