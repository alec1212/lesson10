package dto;

import entities.Role;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.commons.beanutils.BeanUtils;

@Getter
@Setter
public class RoleDto extends Role {
    private Boolean isChecked;

    @SneakyThrows
    public RoleDto(Role role) {
        BeanUtils.copyProperties(this, role);
        this.isChecked = false;
    }
}
