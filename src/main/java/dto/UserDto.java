package dto;

import entities.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto extends User {
    private Boolean isChecked;
}
