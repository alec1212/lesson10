package services;

import dao.RoleDao;
import dao.UserDao;
import entities.Role;
import entities.User;
import exceptions.UserNotFoundException;
import lombok.SneakyThrows;
import utils.HashUtil;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class UserService {

    @Inject
    private UserDao userDao;

    @Inject
    private RoleDao roleDao;

    public Boolean isTruePassword(final String login, final String password) throws Exception {

        User user = userDao.getUserByLogin(login);
        if (user == null) throw new UserNotFoundException(String.format("User '%s' not found", login));
        String dbHashPassword = user.getHashPassword();
        String newHashPassword = HashUtil.getHash(password);
        if (dbHashPassword.equals(newHashPassword))
            return true;
        else
            return false;

    }

    public Boolean isExistUserByLogin(final String login) throws Exception {

        if (login == null || login.isEmpty()) return false;
        return userDao.isExist(login);

    }

    public List<User> getUserList() {
        return userDao.findAll();
    }

    @SneakyThrows
    public User createUser(final String login, final String password) {
        String hPassword = HashUtil.getHash(password);
        return userDao.createUser(login, hPassword);
    }
    @SneakyThrows
    public User editUser(final String id, final String login, final String password)
    {
        User user = new User();
        user.setId(id);
        user.setLogin(login);
        if (password == null || password.isEmpty())
        {
            user.setHashPassword(userDao.getUserById(id).getHashPassword());
        }
        else
        {
            user.setHashPassword(HashUtil.getHash(password));
        }
        userDao.mergeUser(user);
        return user;
    }

    public User removeUser(final String id)
    {
        User user = userDao.getUserById(id);
        if(user == null) return null;
        userDao.removeUser(user);
        return user;
    }

    public User getUserById(final String id)
    {
        if(id == null) return null;
        return userDao.getUserById(id);
    }

    public User getUserByLogin(final String login)
    {
        if(login == null) return null;
        return userDao.getUserByLogin(login);
    }

    public void setRoles(final String userId, final List<String> rolesId)
    {
        User user = userDao.getUserById(userId);
        List<Role> roles = roleDao.findAll()
                .stream()
                .filter(role -> rolesId.contains(role.getId()))
                .collect(Collectors.toList());
        user.setRoles(roles);
        userDao.mergeUser(user);
    }

}
