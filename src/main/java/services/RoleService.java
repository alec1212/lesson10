package services;

import dao.RoleDao;
import entities.Role;
import entities.User;
import lombok.SneakyThrows;
import utils.HashUtil;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class RoleService {

    @Inject
    private RoleDao roleDao;


    public List<Role> getRoleList() {
        return roleDao.findAll();
    }


    @SneakyThrows
    public Role createRole(final String login) {
        return roleDao.createRole(login);
    }

    @SneakyThrows
    public Role editRole(final String id, final String name)
    {
        Role role = new Role();
        role.setId(id);
        role.setName(name);
        roleDao.mergeRole(role);
        return role;
    }

    public Role removeRole(final String id)
    {
        Role role = roleDao.getRoleById(id);
        if(role == null) return null;
        roleDao.removeRole(role);
        return role;
    }

    public Role getRoleById(final String id)
    {
        if(id == null) return null;
        return roleDao.getRoleById(id);
    }

}
