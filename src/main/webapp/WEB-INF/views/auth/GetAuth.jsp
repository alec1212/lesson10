<%@taglib prefix="body" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<body:wrapper>

    <h2 class="text-muted">Авторизация</h2>
    <h5>
        Введите имя и пароль:
    </h5>
    <p>
    <form action="auth" method="post">

        <div class="form-group">
            <label for="exampleInputLogin">Логин</label>
            <input type="login" name="login" value="${login}"
                   class="form-control ${errors.containsKey("login")?"is-invalid":""}"
                   id="exampleInputLogin" aria-describedby="loginHelp" required>
            <small id="loginHelp" class="form-text text-muted">Введите логин указанный при регистрации</small>
            <div class="invalid-feedback">${errors.get("login")}</div>

        </div>

        <div class="form-group">
            <label for="exampleInputPasswword">Пароль</label>
            <input type="password" name="pass" class="form-control ${errors.containsKey("pass")?"is-invalid":""}" id="exampleInputPasswword"
                   aria-describedby="passwordHelp" required>
            <small id="passwordHelp" class="form-text text-muted">Храните свой пароль в тайне.</small>
            <div class="invalid-feedback">${errors.get("pass")}</div>

        </div>

        <input type="hidden" name="pageFrom" value="${pageFrom}">

        <button type="submit" class="btn btn-primary btn-block" value="submit">Да</button>

    </form>
    </p>

</body:wrapper>