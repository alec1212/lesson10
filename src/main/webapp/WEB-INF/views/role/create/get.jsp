<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="body" tagdir="/WEB-INF/tags" %>

<body:wrapper>

    <h2 class="text-muted">Создание роли</h2>
    <h5>
        Введите наименование новой роли:
    </h5>
        <form action="create" method="post">
            <div class="form-group">
                <label for="exampleInputRole">Наименование роли</label>
                <input type="text" name="name" class="form-control ${errors.containsKey("name")?"is-invalid":""}"
                       id="exampleInputRole" aria-describedby="roleHelp" required>
                <small id="roleHelp" class="form-text text-muted"></small>
                <div class="invalid-feedback">${errors.get("name")}</div>

            </div>

            <button type="submit" class="btn btn-primary btn-block" value="submit">Да</button>

    </form>

</body:wrapper>