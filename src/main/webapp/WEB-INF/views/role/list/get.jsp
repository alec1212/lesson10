<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="body" tagdir="/WEB-INF/tags" %>

<body:wrapper>
    <h2 class="text-muted">Список ролей</h2>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">Наименование</th>
            <th scope="col">Действие</th>
        </tr>
        </thead>
        <c:forEach items="${roles}" var="role">
        <tbody>
        <tr>
            <th scope="row">${role.id}</th>
            <td>${role.name}</td>
            <td><a href="../role/edit/?id=${role.id}">Изменить</a>
                <br/>
                <a href="../role/remove/?id=${role.id}">Удалить</a>
            </td>
        </tr>
        </c:forEach>
        </tbody>
    </table>

    <button type="button" class="btn btn-primary btn-block" value="New role" onClick="location.href='../role/create'">Новый
    </button>


</body:wrapper>