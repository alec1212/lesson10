<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="body" tagdir="/WEB-INF/tags" %>

<body:wrapper>

    <h2 class="text-muted">Список пользователей</h2>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">login</th>
            <th scope="col">Действие</th>
        </tr>
        </thead>
        <tbody>

        <c:forEach items="${users}" var="user">
            <tr>
                <th scope="row">${user.id}</th>
                <td>${user.login}</td>
                <td><a href="../user/edit/?id=${user.id}">Изменить</a>
                    <br/>
                    <a href="../user/remove/?id=${user.id}">Удалить</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>

    </table>
    <button type="button" class="btn btn-primary btn-block" value="New user" onClick="location.href='../user/create'">Новый
    </button>


</body:wrapper>