<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="body" tagdir="/WEB-INF/tags" %>

<body:wrapper>
    <h2 class="text-muted">Новый пользователь</h2>
    <h5>     Введите логин и пароль   </h5>


    <form action="create" method="post">

        <div class="form-group">
            <label for="exampleInputLogin">Логин</label>
            <input type="login" name="login" class="form-control ${errors.containsKey("login")?"is-invalid":""}"
                   id="exampleInputLogin" value = "${login}"
                   aria-describedby="loginHelp" required>
            <small id="loginHelp" class="form-text text-muted">Придумайте уникальное имя</small>
            <div class="invalid-feedback">${errors.get("login")}</div>

        </div>

        <div class="form-group">
            <label for="exampleInputPassword">Пароль</label>
            <input type="password" name="pass" class="form-control ${errors.containsKey("pass")?"is-invalid":""}"
                   id="exampleInputPassword"
                   aria-describedby="passwordHelp" required>
            <small id="passwordHelp" class="form-text text-muted">Сохраняйте пароли втайне</small>
            <div class="invalid-feedback">${errors.get("pass")}</div>

        </div>

        <div class="form-group">
            <label for="exampleInputRePassword">Повтор ввода пароля</label>
            <input type="password" name="repPass" class="form-control ${errors.containsKey("repPass")?"is-invalid":""}"
                   id="exampleInputRePassword"
                   aria-describedby="rePasswordHelp" required>
            <small id="rePasswordHelp" class="form-text text-muted">Необходимо для подтверждения правильности ввода</small>
            <div class="invalid-feedback">${errors.get("repPass")}</div>

        </div>

        <button type="submit" class="btn btn-primary btn-block" value="submit">Да</button>

    </form>


</body:wrapper>